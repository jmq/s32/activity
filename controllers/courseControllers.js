const Course = require('../models/course')

module.exports.addCourse = (reqBody) => {	
	let newCourse =  new Course ({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		isActive: reqBody.isActive
	})
	return newCourse.save().then((course, error) => {
		if(error) {
			return true
		} else {
			return course
		}
	})
}


//getAllCourses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result
	})
}


//Retrieve Acttive courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

//Retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result
	})
}


//Update course
module.exports.updateCourse = (data) => {
	console.log(data)
	return Course.findById(data.courseId).then((result, error) => {
		console.log(result)
		if(data.isAdmin) {
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price
			result.isActive = data.updatedCourse.isActive

			console.log(result)
			return result.save().then((updatedCourse, error) => {
				if(error) {
					return false 					
				} else {
					return updatedCourse
				}
			})
		} else {
			return 'Not Admin.'
		}
	})
}


//Update course XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
module.exports.updateCourseX = (data) => {
	console.log(data)
	return Course.findById(data.courseId).then((result, error) => {
		console.log('Before', result)
		
		result.name = data.updatedCourse.name
		result.description = data.updatedCourse.description
		result.price = data.updatedCourse.price
		result.isActive = data.updatedCourse.isActive

		console.log('After', result)
		return result.save().then((updatedCourse, error) => {
			if(error) {
				return false 					
			} else {
				return updatedCourse
			}
		})
	})
}


//Archive course
module.exports.archiveCourse = (data) => {
	console.log(data)
	return Course.findById(data.courseId).then((result, error) => {
		console.log(result)
		if(data.isAdmin) {
			result.isActive = false

			console.log(result)
			return result.save().then((updatedCourse, error) => {
				if(error) {
					return false 					
				} else {
					return updatedCourse
				}
			})
		} else {
			return 'Not Admin.'
		}
	})
}

//Using middleware
module.exports.archiveCourseY = (data) => {
	console.log(data)
	return Course.findById(data.courseId).then((result, error) => {
		console.log(result)
		
			result.isActive = false

			console.log(result)

			return result.save().then((updatedCourse, error) => {
				if(error) {
					return false 					
				} else {
					return updatedCourse
				}
			})
		
	})
}