const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'First name is required.']
	},
	lastName: {
		type: String,
		required: [true, 'Last name is required.']	
	},
	email: {
		type: String,
		min: 5,
		required: true,
		lowercase: true,
		unique: [true, 'email exists already. Please provide unique email']
	},
	password: {
		type: String,
		required: [true, 'Password is required! duh!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	},
	mobileNo: {
		type: String,
		min: 10
	},
	enrollments: [{
		courseId: {
			type: String, 
			required: [true, 'User should be enrolled on at least 1 course.']
		},
		enrolledOn: {
			type: Date,
			default: () => new Date()
		},
		status: {
			type: String,
			default: 'Enrolled'
		}		
	}]
}, {collection: 'users'})

module.exports = mongoose.model('User', userSchema)