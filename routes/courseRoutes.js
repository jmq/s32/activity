const express = require('express')
const router = express.Router()
const courseController = require('../controllers/courseControllers')
const auth = require('../auth')

//Route for creating a course
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	
	if(userData.isAdmin) {
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	} else {
		res.send('Not an Admin.')
	}
})

//Route for retrieving all the courses
router.get('/all', auth.verify, (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})


//Route for retrieving all active courses
router.get('/', (req, res) => {
	courseController.getActiveCourses().then(resultFromController => res.send(resultFromController))
})


//Retrieve specific course -by ID
router.get('/:courseId', (req, res) => {
	console.log(req.params.courseId)

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})

//Update Course
router.put('/:courseId', auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin,
		updatedCourse: req.body
	}

	courseController.updateCourse(data).then(resultFromController => res.send(resultFromController))
})

//Archive course
router.put('/:courseId/archive', auth.verify, (req, res) => {
	const data = {
		courseId: req.params.courseId,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	courseController.archiveCourse(data).then(resultFromController => res.send(resultFromController))
})

//Update Course using middleware XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
router.put('/x/:courseId/archive', auth.verify, auth.isAdmin, (req, res) => {
	const data = {
		courseId: req.params.courseId
	}

	courseController.archiveCourseY(data).then(resultFromController => res.send(resultFromController))
})

module.exports = router
